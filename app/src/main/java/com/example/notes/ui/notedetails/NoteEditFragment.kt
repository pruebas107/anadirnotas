package com.example.notes.ui.notedetails

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.notes.R
import com.example.notes.core.Resource
import com.example.notes.data.local.AppDatabase
import com.example.notes.data.local.LocalDataSource
import com.example.notes.data.model.Note
import com.example.notes.data.remote.ApiClient
import com.example.notes.data.remote.NoteDataSource
import com.example.notes.databinding.FragmentNoteEditBinding
import com.example.notes.presentation.NoteViewModel
import com.example.notes.presentation.NoteViewModelFactory
import com.example.notes.repository.NoteRepositoryImp


class NoteEditFragment : Fragment(R.layout.fragment_note_edit) {

    private lateinit var binding: FragmentNoteEditBinding

    private val viewModel by viewModels<NoteViewModel>{
        NoteViewModelFactory(
            NoteRepositoryImp(
            LocalDataSource(AppDatabase.getDatabase(this.requireContext()).noteDao()),
            NoteDataSource(ApiClient.service)
        ))
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentNoteEditBinding.bind(view)

        binding.btnAddNote.setOnClickListener { view->
            var title: String = binding.editTitle.text.toString()
            var comment: String = binding.editContent.text.toString()
            var image: String = binding.editImageUrl.text.toString()
            var Add: Note = Note("", title, comment, image)

            viewModel.saveNotes(Add).observe(viewLifecycleOwner, Observer { result ->
                when(result){
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        Toast.makeText(context, "Nota añadida", Toast.LENGTH_SHORT).show()
                    }
                    is Resource.Failure -> {
                        Toast.makeText(context, "Error al añadir nota", Toast.LENGTH_SHORT).show()

                    }
                }
            })

        }
    }

}