package com.example.notes.data.model

import android.widget.EditText

data class Note(
    val id:String = "",
    val title: String = "",
    val content:String = "",
    val image:String = ""
    )